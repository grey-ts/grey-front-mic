
import { ConfigProvider } from "antd";
import zhCN from 'antd/lib/locale/zh_CN';
import { Dev } from 'grey-mic-dev';
import 'grey-mic-debug-info';

import React from 'react';
import ReactDOM from 'react-dom';
import * as GreyMic from 'grey-mic';
import * as antd from 'antd';

GreyMic.Genie.config = Dev.LOCAL_CONFIG;
window.$_MIC = { React, ReactDOM, antd, GreyMic };

export default function App() {
  return (
    <ConfigProvider locale={zhCN}>
        <Dev />
    </ConfigProvider>
  );
}

