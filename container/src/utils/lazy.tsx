
import React from 'react';

const lazy = (factory: () => Promise<any>, load = <></>) => props => {
    const [Ct, setCt] = React.useState<any>();
    React.useEffect(() => {
        (async () => {
            const Ct = await factory();
            setCt(Ct);
        })();
    }, []);
    // eslint-disable-next-line react/jsx-pascal-case
    return (Ct && Ct.default) ? <Ct.default {...props} /> : load;
};

export default lazy;