
import React from 'react';
import ReactDOM from 'react-dom';
import './index.less';
import lazy from './utils/lazy'
import 'antd/dist/antd.css';

const App = lazy(() => import('./App'));

ReactDOM.render(
    <React.StrictMode>
        <App />
    </React.StrictMode>,
    document.getElementById('root')
);

