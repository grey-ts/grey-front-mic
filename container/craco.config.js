
const CracoLessPlugin = require("craco-less");
const path = require("path");

module.exports = {
    plugins: [
        { plugin: CracoLessPlugin }
    ],
    webpack: {
        configure: (webpackConfig, { env, paths }) => {
            webpackConfig.externals = {
                "app-template-2": "window['app-template-2'][0]",
                'React': path.resolve(process.cwd(), 'node_modules', 'react'),
            }
            return webpackConfig;
        }
    }
};