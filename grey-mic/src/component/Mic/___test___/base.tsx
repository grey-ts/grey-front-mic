
import React from 'react';
import { Mic, Genie } from '../../../'

try {
    Genie.config = eval(`(${localStorage.getItem('$/MIC/CONFIG')})`);
} catch (error) {
    Genie.config = {};
}
 
export default () => {

    return (
        <Mic id="local.dev" />
    );
};
