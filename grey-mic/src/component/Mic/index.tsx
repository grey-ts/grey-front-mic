
import React from "react";
import classnames from "classnames";
import Genie from "../Genie";
import IMipProps from "./interfaces/IMipProps";
import './index.less';

/** 
 * MIC 组件 
 * 
 * 通过精灵 获取 MIC组件 并执行渲染
 * */
export default function Mic(props: IMipProps) {
    
    /** 精灵 */
    const genie = React.useMemo(() => Genie.getGenie(props.id), [props.id]);

    const pack = genie.useMemo(s => s.pack, []);

    const Skin = pack[props.moduleName || 'default'];

    return (
        <span data-mic-id={props.id} className={classnames('grey-mic', props.className)} style={{ ...props.style }}>
            {Skin
                ? (
                    <Skin {...props.appProps} history={props.history} >
                        {props.children}
                    </Skin>
                )
                : <>{props.spin}</>
            }
        </span>
    )
}


