
import React from "react";
import { History } from 'history';

export default interface IMipProps {

    /** 应用标识 */
    id: string;

    /** 样式名 (应用实现) */
    className?: string;

    /** 样式 (应用实现)  */
    style?: React.CSSProperties;

    /** 传递给应用的参数 */
    appProps?: any;

    /** 模块名称 */
    moduleName?: string;

    /** 加载器 */
    spin?: React.ReactChild;

    /** 路由 */
    history?: History<any>;

    /** 孩子 */
    children?: React.ReactNode;
}


