
import ReactBox from 'grey-react-box'
import React from 'react';
import superagent from 'superagent';
import Aisle from '../Aisle';

/** 应用配置 */
export interface IGenieConfig {

    [key: string]: any;

    /** 应用ID */
    id: string;

    /** 用户地址 */
    url: string;
}

/**
 * MIC精灵 状态 
 */
export class GenieState {

    /** 是否初始化完成 */
    isInit = false;

    /** 公开组件 */
    pack: { [key: string]: React.ComponentType<any> } = {};
}

/** 
 * MIC精灵 
 * 管理 JS CSS 的加载，组件对象的管理
 * */
export default class Genie extends ReactBox<GenieState> {

    /** 精灵实例字典 */
    static readonly genieMap: { [key: string]: Genie | undefined } = {};

    /** 应用配置 */
    static config: { [key: string]: IGenieConfig | undefined } = {};

    /** 设置应用配置 */
    static setConfig(value: { [key: string]: { url: string, [key: string]: any } }) {
        window['$_Mic_Config'] = Genie.config = {};
        for (let key in value) {
            Genie.config[key] = { ...value[key], id: key };
        }
    }

    /** 用户ID */
    readonly id: string;

    /** 通道 */
    readonly aisle: Aisle & { [key: string]: any };

    /** 获取一个精灵 */
    static getGenie = (id: string) => Genie.genieMap[id] || new Genie(id);

    /** 构造 */
    constructor(id: string) {
        super(new GenieState());
        if (Genie.genieMap[id]) {
            throw new Error("精灵重复定义");
        } else {
            Genie.genieMap[id] = this;
            this.id = id;
            this.aisle = new Aisle();
            this.init();
        }
    }

    /** 应用的根路径 */
    get url() {
        return Genie.config[this.id]?.url;
    }

    /** 精灵配置 */
    get config() {
        return Genie.config[this.id];
    }

    /** 加载资源 */
    private init = async () => {
        // let entrypoints = this.config?.entrypoints.slice() || [];
        const url = this.config?.url || '';

        console.log('[MIC LOAD] ===> load', `[${this.id}]`, this.config);

        const entrypoints: string[] = await (async () => {
            try {
                const data = await superagent.get(`${url}/asset-manifest.json?${new Date().getTime()}`);
                return data.body.entrypoints || [];
            } catch (error) {
                return [];
            }
        })()

        entrypoints.forEach(e => {
            const src = `${url}/${e}`;
            const onload = () => {
                entrypoints.splice(entrypoints.indexOf(e), 1);
                if (entrypoints.length <= 0) {
                    this.pipeline(async s => {
                        const micInfo = window['$_MicInit'](this) || {};
                        this.aisle['boxState'] = micInfo.state || {};
                        s.pack = micInfo.pack || {};
                        s.isInit = true;
                        console.log('[MIC LOAD] ===> end', s.pack);
                    })();
                }
            }

            if (src.split('.').pop() === 'css') {
                const link = document.createElement('link');
                link.onload = onload;
                link.href = src;
                link.rel = "stylesheet";
                document.head.append(link);
            } else {
                const script = document.createElement('script');
                script.onload = onload;
                script.src = src;
                document.head.append(script);
            }
        })
    }
}



