
import GreyReactBox from 'grey-react-box';

/** 
 * 通道 
 * 应用与应用 、皮肤与精灵 的通信通道
 * */
export default class Aisle extends GreyReactBox<any> {
    constructor(state = {}) {
        super(state);
    }
}