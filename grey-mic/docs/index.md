
## 简介

基于 create-react-app 微前端框架

快速应用请看 Mic

## Mic 子应用 配置

```tsx
import React from 'react';
import { Input } from 'antd';
import 'antd/dist/antd.css';

const defaultConfig = `{
    "local.dev":{
        id: 'local.dev',
        url: '//localhost:8888',
        directions: '本地开发',
        entrypoints: [
            "static/js/runtime-main.js",
            "static/js/vendors~main.app.chunk.js",
            "static/js/main.app.chunk.js"
        ]
    }
}`

export default () => {

    const [value, setValue] = React.useState<string | null>(localStorage.getItem('$/MIC/CONFIG') || defaultConfig);

    React.useEffect(() => {
        window.localStorage.setItem('$/MIC/CONFIG', value || '');
    }, [value])

    return (
        <>
            <p>设置 独立部署 token</p>
            <Input.TextArea
                rows={20}
                value={value || ''}
                onChange={e => { setValue(e.target.value) }}
            />
        </>
    )
};
```