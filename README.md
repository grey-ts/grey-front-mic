# grey-front-mic 
基于 create-react-app 微前端框架。
实用云+端模式的前端，端侧支撑不同客户提供非标准产品的定制扩展

## 技术思路
* 利用webpack的运行此做沙箱隔离
* css module 做的样式隔离

## 项目要求
* 容器项目和子项目需要用统一的 react、react-dom、antd

## 目录说明
```
| project
└───| container                 //容器模板   
                                //
    | app-template              //子应用模板
    └───| public                //静态资源
        | src                   //
            | pages             //页面   
            └───| Demo1         //页面示例
                | Demo2         //页面示例
            | utils             //工具包
            └───| lazy.tsx      //按需加载工具
            | App.tsx           //一个简单应用的实现 应用得根路由也在这里配置
            | index.tsx         //应用入口
        | typings.d.ts          //TS的描述文件
```

## 创建子项目

直接复制 app-template 修改  package.json 的 name <br/>
注意：所有子项目的 name 不能重复