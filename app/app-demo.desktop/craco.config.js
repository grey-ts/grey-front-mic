
const CracoLessPlugin = require("craco-less");

module.exports = {
    plugins: [
        { plugin: CracoLessPlugin }
    ],
    webpack: {
        configure: (webpackConfig, { env, paths }) => {
            webpackConfig.externals = {
                "react": "$_MicWindow.$_MIC.React",
                "react-dom": "$_MicWindow.$_MIC.ReactDOM",
                "antd": "$_MicWindow.$_MIC.antd",
                "Mic": "$_MicWindow.$_MIC.Mic",
                "aisle": "$_MicAisle",
            }
            return webpackConfig;
        }
    }
}