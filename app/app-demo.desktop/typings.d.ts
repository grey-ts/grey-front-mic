
declare module '*.css';
declare module '*.less';
declare module '*.svg';

declare module 'Aisle' {
    /** 通信通道 */
    import GreyReactBox from 'grey-react-box';
    import AisleState from 'AisleState';
    export interface MipProps {

        /** 应用标识 */
        id: string;

        /** 样式名 */
        className?: string;

        /** 样式 */
        style?: React.CSSProperties;

        /** 传递给app的参数 */
        appProps?: any;

        /** 模块名称 */
        moduleName?: string;

        /** 加载器 */
        spin?: React.ReactChild;

        /** 路由 */
        history?: History;

        /** 孩子 */
        children?: React.ReactNode
    }
    export interface IAisleOptions {
        /** app id */
        id: string;
        /** app 的window */
        appWindow: Window;
        /** app 的容器 */
        appHead: HTMLElement;
    }
    export default class Aisle extends GreyReactBox<AisleState & { [key: string]: any }> {
        /** sdk 的容器 */
        top: Window;
        /** app 的window */
        appWindow: Window;
        /** app 的容器 */
        appHead: HTMLElement;
        /** 应用ID */
        id: string;
        constructor(options: IAisleOptions);
        get micPack(): any;
        /** 获取 指定ID 的iframe */
        iframe: (id?: string) => HTMLIFrameElement;
        /** 获取指定应用的通道 */
        getAisle: (id?: string) => Aisle;
        /** 切换前台应用 */
        switchFrontDesk: (mic: MipProps) => Promise<void>;
        /** 切换到后台 */
        switchBackground: () => Promise<void>;
        /** 调用应用的方法 */
        call: (id: string, fnName: string, ...parameter: any[]) => Promise<any>;
        /** 转移CSS */
        moveCss: () => Promise<void>;
    }
}

declare module 'aisle' {
    import Aisle from 'Aisle';
    const _default: Aisle;
    export default _default;
}

declare module 'Mic' {
    import { History } from "history";
    import React from "react";
    export interface MipProps {
        /** 应用标识 */
        id: string;
        /** 样式名 */
        className?: string;
        /** 样式 */
        style?: React.CSSProperties;
        /** 传递给app的参数 */
        appProps?: any;
        /** 模块名称 */
        moduleName?: string;
        /** 加载器 */
        spin?: React.ReactChild;
        /** 路由 */
        history?: History;
        /** 孩子 */
        children?: React.ReactNode;
    }
    export default function Mic(props: MipProps): JSX.Element;
}

interface Window {
    /** 根容器 window */
    $_MicWindow: Window & { $_MIC: any },

    /** 应用暴露的包 */
    $_MicPack: any;

    /** 通道 */
    $_MicAisle: any;

    /** 公开的方法 */
    $_Export: any;

    /** 应用全局数据 */
    $_AisleState: any;
}