
import { createMemoryHistory } from "history";
import { Router, Route, Switch } from "react-router-dom";
import lazy from 'utils/lazy';
import aisle from 'aisle';
import React from "react";

const Desktop = lazy(() => import('pages/Desktop'));

export default function App(props) {

  const token = aisle.getAisle('demo.base').useMemo(s => s.token, []);

  React.useEffect(() => {
    if (!token) aisle.switchFrontDesk({ id: 'demo.base' })
  }, [token])

  return (
    <Router history={props.history || createMemoryHistory()} >
      {
        token
          ? (
            <Switch>
              <Route path="/" component={Desktop} />
            </Switch>
          )
          : <></>
      }
    </Router>
  );
}