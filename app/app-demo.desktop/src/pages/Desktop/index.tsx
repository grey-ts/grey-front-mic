
import { Button } from 'antd';
import { ExportOutlined } from '@ant-design/icons';
import styles from './index.module.less';
import React from 'react';
import aisle from 'aisle';

export default function Desktop() {

    const onQuit = React.useCallback(() => {
        aisle.call('demo.base', 'signOut')
    }, [])

    return (
        <div className={styles.desktop} >
            <div className={styles.top} >
                <div className={styles.left} ></div>
                <div className={styles.right} >
                    <Button onClick={onQuit} type="link" icon={<ExportOutlined />} />
                </div>
            </div>
            <div className={styles.main} >
                <div className={styles.menu} >左</div>
                <div className={styles.cont} >cont</div>
            </div>
        </div>
    );
}

