
import { Button, Card, Checkbox, Form, Input } from 'antd';
import React from 'react';
import styles from './index.module.less';
import aisle from 'aisle';

export default function Login() {

    const onFinish = React.useCallback(() => {
        aisle.pipeline(async s => {
            s.token = 'xxxxxxxx';
            window.localStorage.setItem('@/user/token', s.token);
        })()
    }, [])

    return (
        <div className={styles.login} >
            <Card title="用户登录" className={styles.card} >
                <Form
                    name="basic"
                    labelCol={{ span: 8 }}
                    wrapperCol={{ span: 16 }}
                    initialValues={{ remember: true }}
                    autoComplete="off"
                    onFinish={onFinish}
                >
                    <Form.Item
                        label="用户名"
                        name="username"
                        rules={[{ required: true, message: 'Please input your username!' }]}
                    >
                        <Input />
                    </Form.Item>

                    <Form.Item
                        label="密码"
                        name="password"
                        rules={[{ required: true, message: 'Please input your password!' }]}
                    >
                        <Input.Password />
                    </Form.Item>

                    <Form.Item name="remember" valuePropName="checked" wrapperCol={{ offset: 8, span: 16 }}>
                        <Checkbox>记住登录状态</Checkbox>
                    </Form.Item>

                    <Button type="primary" block htmlType="submit">
                        登录
                    </Button>
                </Form>
            </Card>
        </div>
    );
}
