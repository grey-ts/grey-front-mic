
import React from 'react';
import aisle from 'aisle';

export default function Login() {

    React.useEffect(() => {
        (async () => {
            await aisle.switchBackground();
            await aisle.switchFrontDesk({ id: 'demo.desktop.menu' });
        })()
    })

    return (
        <></>
    );
}
