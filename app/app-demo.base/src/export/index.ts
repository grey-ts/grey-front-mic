
export const onLoadApp = async (id: string) => {
}

export const signOut = async () => {
    const aisle = (await import('aisle')).default;
    aisle.pipeline(async s=>{
        s.token = undefined;
        window.localStorage.removeItem('@/user/token');
    })()
}
