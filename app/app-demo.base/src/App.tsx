
import { createMemoryHistory } from "history";
import { Router, Route, Switch } from "react-router-dom";
import lazy from 'utils/lazy';
import aisle from 'aisle';

const Desktop = lazy(() => import('pages/Desktop'));
const Login = lazy(() => import('pages/Login'));

export default function App(props) {

  const token = aisle.useMemo(s => s.token, []);

  return (
    <Router history={props.history || createMemoryHistory()} >
      {
        token ? (
          <Switch>
            <Route path="/" component={Desktop} />
          </Switch>
        ) : (
          <Switch>
            <Route path="/" component={Login} />
          </Switch>
        )
      }
    </Router>
  );
}