
import lazy from 'utils/lazy';

window.$_Export = require('./export');
window.$_AisleState = require('./AisleState').default;
window.$_MicPack = {
  default: lazy(() => import('./App')),
}
