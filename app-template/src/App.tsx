
import { Router, Route, Redirect, Switch } from "react-router-dom";
import Controller, { MyContext } from "Controller";
import { createMemoryHistory, History } from "history";
import Demo1 from 'pages/Demo1';
import Demo2 from 'pages/Demo2';
import React from "react";

export default function App(props: { history?: History }) {

  const controller = React.useMemo(()=>new Controller(), [])

  return (
    <MyContext.Provider value={controller} >
      <Router history={createMemoryHistory()}>
        <Switch>
          <Route path={`/home/systemManage/tab/demo1`} component={Demo1} />
          <Route path={`/home/systemManage/tab/demo2`} component={Demo2} />
          <Route render={() => <Redirect to={`/home/systemManage/tab/demo1`} />} />
        </Switch>
      </Router>
    </MyContext.Provider>
  );
}
