import React from 'react';
import ReactBox from 'grey-react-box';
import ControllerState from './ControllerState';
import { fnDemo } from './fns/fnDemo';

export const MyContext = React.createContext<Controller>(null as any);

/**
 * 当前项目的全局数据
 */
export default class Controller extends ReactBox<ControllerState> {
    
    constructor() {
        super(new ControllerState())
    }

    fnDemo = this.pipeline(fnDemo);
}