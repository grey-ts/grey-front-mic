
import ControllerState from "Controller/ControllerState";

/**
 * 数据管理demo
 * @param box 当前状态
 * @param opt 入参-注意，opt 必须是可选参数
 */
export async function fnDemo(box: ControllerState, opt?: number) {
    box.no += opt || 0;
}