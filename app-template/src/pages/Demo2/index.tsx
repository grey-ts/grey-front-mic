
import styles from './index.module.less'
import { Mic } from 'grey-mic';
import React from 'react';

export default function App() {
    return (
        <div className={styles.demo2} >
            <p>demo2</p>
            <Mic id="local.dev" />
        </div>
    );
}
