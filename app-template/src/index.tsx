
import AisleState from 'AisleState';
import App from 'App';
import { Genie } from 'grey-mic';
import micEnv from 'micEnv';

window.$_MicInit = (genie: Genie) => {
  micEnv.genie = genie;
  return {
    state: new AisleState(),
    pack: {
      default: App,
    }
  }
}

