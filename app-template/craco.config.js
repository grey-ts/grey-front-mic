
const CracoLessPlugin = require("craco-less");

module.exports = {
    hash: false,
    plugins: [
        { plugin: CracoLessPlugin }
    ],
    webpack: {
        configure: (webpackConfig, { env, paths }) => {
            webpackConfig.externals = {
                "react": "$_MIC.React",
                "react-dom": "$_MIC.ReactDOM",
                "antd": "$_MIC.antd",
                "grey-mic": "$_MIC.GreyMic",
            }
            // webpackConfig.output = {
            //     ...webpackConfig.output,
            //     filename: 'static/js/[name].js',
            //     chunkFilename: 'static/js/[name].app.chunk.js',
            // }
            return webpackConfig;
        }
    }
}